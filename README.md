# MCjetpacks

This datapack adds jetpacks to Minecraft, which can be obtained in a crafting table in survival mode or via a command.

## Installation:
You need both a resource pack and datapack. The resource pack provides the textures and models, and the datapack adds the crafting and functionality.
To install the datapack, go to the file [/build/jetpacks-data.zip](https://gitlab.com/pear_man/mcjetpacks/-/raw/main/build/jetpack-data.zip) in the repo.
The resource pack is at [/build/jetpacks-resource.zip](https://gitlab.com/pear_man/mcjetpacks/-/raw/main/build/jetpack-resource.zip) in the repo.

## Obtaining a jetpack

To obtain a jetpack in creative mode, simply type the command


    /function jetpacks:get


To get a jetpack in survival mode, use the crafting recipe.
The crafting recipe is as following:

![crafting recipe](https://pearnet.onrender.com/jp-recipe.png)


Both methods will craft a jetpack handle, which when held will cause the user to be wearing a jetpack on their back.

## Usage
To use the jetpack, hold the jetpack handle in your hand. It will show a jetpack on your back. To fly, right click. The slow falling effect will be applied to any user holding a jetpack handle that is one block above the ground. This functionality sometimes does not work, but you will usually not take any fall damage.


⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ ⚠️ 


**WARNING: ANY ITEMS ON THE JETPACK USER'S HEAD SLOT WILL BE CLEARED AND DELETED FOREVER WHEN THE JETPACK HANDLE IS HELD!**

*MAKE SURE TO CLEAR YOUR HEAD SLOT BEFORE HOLDING THE JETPACK HANDLE!!*

